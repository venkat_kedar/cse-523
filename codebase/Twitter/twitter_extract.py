import tweepy
from twitterpandas import TwitterPandas
from keys import con_key,con_secret,access_token,access_token_secret
import json
import pickle
import requests
import re
import httplib
from urlparse import urlparse
from urllib2 import Request, urlopen, URLError


def load_api(con_key,con_secret,access_token,access_token_secret):
    auth=tweepy.OAuthHandler(consumer_key=con_key,consumer_secret=con_secret)
    auth.set_access_token(access_token, access_token_secret)
    return tweepy.API(auth)

def unshorten_url(url):
    parsed = urlparse(url)
    h = httplib.HTTPConnection(parsed.netloc)
    try:
        h.request('HEAD', parsed.path)
        response = h.getresponse()
        if response.status/100 == 3 and response.getheader('Location'):
            return response.getheader('Location')
        else:
            return url
    except:
        pass


def validate_url(url):
    req=Request(url)
    try:
        response=urlopen(req)
    except URLError,e:
        if hasattr(e,'reason'):
             print 'Reason: ', e.reason
        elif hasattr(e,'code'):
            print 'Error code: ', e.code
    else:
        print response


def make(api,query,count):
    tweet_list=[]
    for tweet in tweepy.Cursor(api.search,query,include_entities=True,lang="en").items(count):
        tweet_text=tweet.text.encode('ascii','ignore')
        screen_name=tweet.user.screen_name.encode('ascii')
        hashtags=[i  for i in tweet_text.split() if i.startswith("#") ]
        mentions=[i for i in tweet_text.split() if i.startswith("@")]
        url_list_temp=re.findall(r'(https?://\S+)', tweet_text)
        url_list=[x for x in url_list_temp if x!="https://t.co"]
        url_list=[unshorten_url(url) for url in url_list_temp]
        if len(url_list)==0:
            tweet_data=(screen_name,tweet.text.encode('ascii','ignore'),hashtags,mentions)
        else:
            tweet_data=(screen_name,tweet.text.encode('ascii','ignore'),hashtags,mentions,url_list)
        tweet_list.append(tweet_data)
    return tweet_list


def write_pickle(tweet_list,filename):
    with open(filename,"wb") as fp:
        pickle.dump(filename,fp)
    return filename
def load_pickle(filename):
    with open(filename,"rb") as fp:
        tweet_list=pickle.load(fp)
    return tweet_list


api=load_api(con_key,con_secret,access_token,access_token_secret)
tweet_list=make(api,"#ptsd",100)
write_pickle(tweet_list,"tweet_list.txt")
