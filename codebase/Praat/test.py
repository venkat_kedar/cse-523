import praatUtil
import os
from matplotlib import pyplot as plt
import matplotlibUtil
import generalUtility
import sys
fName = 'WilhelmScream.wav'

path = sys.path[0] + '/'
fileNameOnly = generalUtility.getFileNameOnly(fName)

dataT, dataI = praatUtil.calculateIntensity(path + fName)

dataI -= dataI.max()

graph = matplotlibUtil.CGraph(width = 8, height = 3)
graph.createFigure()
ax = graph.getArrAx()[0]
ax.plot(dataT, dataI, linewidth = 2)
ax.set_xlabel("Time [s]")
ax.set_ylabel("SPL [dB]")
ax.set_title(fileNameOnly)
graph.padding = 0.1
graph.adjustPadding(bottom = 2, right = 0.5)
ax.grid()

matplotlibUtil.setLimit(ax, dataI, 'y', rangeMultiplier = 0.1)

matplotlibUtil.formatAxisTicks(ax, 'y', 6, '%d')

plt.savefig(fileNameOnly + "_intensity.png")
