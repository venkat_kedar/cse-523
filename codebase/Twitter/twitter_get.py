import tweepy
from twitterpandas import TwitterPandas
from keys import con_key,con_secret,access_token,access_token_secret
import json
import pickle
import requests

def load_api(con_key,con_secret,access_token,access_token_secret):
    auth=tweepy.OAuthHandler(consumer_key=con_key,consumer_secret=con_secret)
    auth.set_access_token(access_token, access_token_secret)
    return tweepy.API(auth)

def query_search(api,query,count):
    tweet_list=[]
    for tweet in tweepy.Cursor(api.search,query,include_entities=True).items(count):
        tweet_tuple=(tweet.user.screen_name.encode('ascii'),tweet.text.encode('ascii','ignore'))
        tweet_list.append(tweet_tuple)
    return tweet_list

def get_url_query(api,query,count):
    tweet_urls=[]
    for tweet in tweepy.Cursor(api.search,query,include_entities=True,lang="en").items(count):
        for url in tweet.entities['urls']:
            expanded_url=url['expanded_url']
            r=requests.head(expanded_url)
            if r.status_code in range(200,300):
                tweet_urls.append(format(r.url))
            elif r.status_code in range(300,400):
                tweet_urls.append(format(r.headers['location']))
            else:
                tweet_urls.append(format(r.status_code))
    return tweet_urls

def write_pickle(tweet_list,filename):
    with open(filename,"wb") as fp:
        pickle.dump(filename,fp)
    return filename
def load_pickle(filename):
    with open(filename,"rb") as fp:
        tweet_list=pickle.load(fp)
    return tweet_list

def get_hashtags(tweet_list):
    tweet_text=[s[1] for s in tweet_list]
    hashtags=[i  for i in tweet_text.split() if i.startswith("#") ]
    hash_dict={}
    for hashtag in hashtags:
        if hashtag in hash_dict:
            hash_dict[hashtag]+=1
        else:
            hash_dict[hashtag]=1
    return hash_dict

def get_mentions(tweet_list):
    tweet_text=[s[1] for s in tweet_list]
    mentions=[i for i in tweet_text.split() if i.startswith("@")]
    mentions_dict={}
    for mention in mentions:
        if mention in mention_dict:
            mention_dict[mention]+=1
        else:
            mention_dict[mention]=1
    return mention_dict


################################# MAIN #############################################



api=load_api(con_key,con_secret,access_token,access_token_secret)
tweet_url=get_url_query(api,"#zlatan",1000)
print tweet_url
#tweet_list=load_pickle("tweet_list.txt")
